Este servicio permite obtener preguntas aleatorias e informar al Servicio de Posicion cuando una pregunta se ha contestado correctamente. Entre más preguntas hayan en este repositorio, se reduce la probabilidad de que se repitan preguntas al jugador. 

**Base de datos en Mongo**

*Esquema Questions*
 
Se requiere inicializar preguntas en una Coleccion llamada **Questions**. La estructura a almacenar es:

```
{
	"id": "5d6496dd1c9d440000e093ec",
	"level": 1,
	"description": "¿Cuànto es 1 + 1?",
	"options":[
		{
			"description": "1",
			"letterOption": "a",
			"correct": false
		},
		{
			"description": "2",
			"letterOption": "b",
			"correct": true
		}
	]
}
```

**Tópico en kafka**

Se requiere crear un topico que registra el evento cuando una pregunta sea contestada de manera correcta. El nombre por defecto del topico es challenge, aunque puede ser cambiado y pasado como argumento cuando se arranque la aplicación. El periodo de retención debe ser corto, se sugiere: 1 segundo.

`kafka-topics.sh --zookeeper 192.168.0.50:2181 --alter --topic challenge --config retention.ms=1000`

**Docker**

La imagen de docker para este microservicio se genera a partir del Siguiente Dockerfile, que hace referencia al paquete jar generado para la última versión en código estable. Durante la ejecución, recibe 12 parametros que asigna a variables de entorno que deben enviarse al momento de levantar el contenedor. En el archivo Dockerfile se incluye valores por defecto que seguramente no funcionarán en su ambiente.

```
FROM openjdk:8u131-jre-alpine
COPY ./challenges-0.0.1-SNAPSHOT.jar app.jar


ENV MEETUP_SERVER_PORT 8082
ENV MEETUP_KAFKA_SERVER	192.168.0.50
ENV MEETUP_KAFKA_PORT	9092
ENV MEETUP_KAFKA_TOPIC_CHALLENGE challenge
ENV MEETUP_MONGO_USER	game
ENV MEETUP_MONGO_PASSWORD "set_pass"
ENV MEETUP_MONGO_SERVER	"set_server"
ENV MEETUP_MONGO_PORT	27017
ENV MEETUP_MONGO_DB game
ENV MEETUP_MONGO_RW true
ENV MEETUP_MONGO_W majority
ENV MEETUP_MONGO_SSTO 2000
ENV MEETUP_MONGO_FORMAT_CONNECTION mongodb

EXPOSE $MEETUP_SERVER_PORT
CMD ["/usr/bin/java", "-jar", "app.jar"]
```

**Build de este Dockerfile**:

`docker build -f Dockerfile -t meetup/preguntas.`

**Run de este Dockerfile**:

`docker run -d --name preguntas -e MEETUP_KAFKA_SERVER=[IP KAFKA] -e MEETUP_KAFKA_PORT=[PUERTO KAFKA] -e MEETUP_KAFKA_TOPIC_CHALLENGE=challenge -e MEETUP_MONGO_USER=[USUARIO USUARIO MONGO] -e MEETUP_MONGO_PASSWORD=[PASSWORD USUARIO MONGO] -e MEETUP_MONGO_SERVER=[IP SERVIDOR MONGO] -e MEETUP_MONGO_PORT=[PUERTO MONGO] -e MEETUP_MONGO_DB=[NOMBRE DE LA BD MONGO] -e MEETUP_MONGO_SSTO=20000 -e MEETUP_MONGO_FORMAT_CONNECTION=mongodb -e MEETUP_SERVER_PORT=8092  -p 8092:8092 meetup/preguntas`

**Argumentos**

* MEETUP_KAFKA_SERVER	Ip del servidor de kafka
* MEETUP_KAFKA_PORT	Puerto para el servidor kafka
* MEETUP_KAFKA_TOPIC_CHALLENGE nombre del topico para las preguntas respondidas correctamente. por defecto challenge
* MEETUP_MONGO_USER	Nombre de usuario para la base de datos mongo
* MEETUP_MONGO_PASSWORD password para autorizar el usuario de la base de datos mongo
* MEETUP_MONGO_SERVER	DNS o ip del servidor
* MEETUP_MONGO_DB nombre de la base de datos en mongo
* MEETUP_MONGO_RW Valor para retryWrites en mongo, por defecto true
* MEETUP_MONGO_W valor para w en mongo, por defecto majority
* MEETUP_MONGO_SSTO valor para serverSelectionTimeoutMS en mongo, por defecto 2000
* MEETUP_MONGO_CTO valor para connectTimeoutMS en mongo, por defecto 2000
* MEETUP_MONGO_FORMAT_CONNECTION formato de conexión. Valores posibles: mongodb, mongodb+srv
* MEETUP_SERVER_PORT Puerto http por el cual se expone este servicio