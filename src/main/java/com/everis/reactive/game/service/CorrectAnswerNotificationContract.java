package com.everis.reactive.game.service;

import com.everis.reactive.game.entitty.dto.CorrectAnswerMessage;

public interface CorrectAnswerNotificationContract {

	public void notifyCorrectAnswer(CorrectAnswerMessage message);

}
