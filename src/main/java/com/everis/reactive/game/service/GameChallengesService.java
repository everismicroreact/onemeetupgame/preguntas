package com.everis.reactive.game.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.everis.reactive.game.entitty.dto.CorrectAnswerMessage;
import com.everis.reactive.game.entitty.dto.OptionsForm;
import com.everis.reactive.game.entitty.dto.PlayerAnswerForm;
import com.everis.reactive.game.entitty.dto.QuestionForm;
import com.everis.reactive.game.repository.QuestionRepository;

@Service
public class GameChallengesService implements GameChallengesServiceContract, CorrectAnswerNotificationContract {

	@Autowired
	private Source source;

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	@SendTo(Source.OUTPUT)
	public void notifyCorrectAnswer(CorrectAnswerMessage message) {
		source.output().send(MessageBuilder.withPayload(message).setHeader("operation", "challenge").build());

	}

	@Override
	public QuestionForm requestForAChallenge() {

		Aggregation agg = newAggregation(sample(1L));
		AggregationResults<QuestionForm> results = mongoTemplate.aggregate(agg, QuestionForm.class, QuestionForm.class);
		QuestionForm question = results.getUniqueMappedResult();

		return question;
	}

	@Override
	public Boolean validateChallengeAnswer(PlayerAnswerForm answer) {

		boolean correctAnswer = questionRepository.findById(answer.getQuestion().getId()).get().getOptions().stream()
				.filter((OptionsForm option) -> option.isCorrect() == true).findFirst()
				.filter(x -> x.getLetterOption().equals(answer.getQuestion().getOptions().get(0).getLetterOption()))
				.isPresent();

		if (correctAnswer) {
			CorrectAnswerMessage message = new CorrectAnswerMessage();
			message.setLevel(1);
			message.setMiliseconds(answer.getMiliseconds());
			message.setPlayer(answer.getPlayer());
			notifyCorrectAnswer(message);
		}

		return Boolean.valueOf(correctAnswer);
	}

}
