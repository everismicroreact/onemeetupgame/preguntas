package com.everis.reactive.game.service;

import com.everis.reactive.game.entitty.dto.PlayerAnswerForm;
import com.everis.reactive.game.entitty.dto.QuestionForm;

public interface GameChallengesServiceContract {

	
	public QuestionForm requestForAChallenge();
	public Boolean validateChallengeAnswer(PlayerAnswerForm answer);
	
	
}
