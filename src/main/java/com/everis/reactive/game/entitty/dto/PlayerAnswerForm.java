package com.everis.reactive.game.entitty.dto;

public class PlayerAnswerForm {
	private QuestionForm question;
	private PlayerForm player;
	private int miliseconds;
	
	public QuestionForm getQuestion() {
		return question;
	}
	public void setQuestion(QuestionForm question) {
		this.question = question;
	}
	public PlayerForm getPlayer() {
		return player;
	}
	public void setPlayer(PlayerForm player) {
		this.player = player;
	}
	public int getMiliseconds() {
		return miliseconds;
	}
	public void setMiliseconds(int miliseconds) {
		this.miliseconds = miliseconds;
	} 

	
	
}
