package com.everis.reactive.game.entitty.dto;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Questions")
public class QuestionForm {
	@Id
	private String id;

	private int level;
	private String description;
	private List<OptionsForm> options;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<OptionsForm> getOptions() {
		return options;
	}

	public void setOptions(List<OptionsForm> options) {
		this.options = options;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
