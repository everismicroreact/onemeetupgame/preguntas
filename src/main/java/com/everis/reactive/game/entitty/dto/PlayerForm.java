package com.everis.reactive.game.entitty.dto;

public class PlayerForm {
	private String user;
	private String identification;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}
}