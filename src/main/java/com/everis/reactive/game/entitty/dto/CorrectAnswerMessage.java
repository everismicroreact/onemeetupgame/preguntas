package com.everis.reactive.game.entitty.dto;

public class CorrectAnswerMessage {
	private int level;
	private int miliseconds;
	private PlayerForm player;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public PlayerForm getPlayer() {
		return player;
	}

	public void setPlayer(PlayerForm player) {
		this.player = player;
	}

	public int getMiliseconds() {
		return miliseconds;
	}

	public void setMiliseconds(int miliseconds) {
		this.miliseconds = miliseconds;
	}
	
	

}
