package com.everis.reactive.game.entitty.dto;

public class OptionsForm {
	private String description;
	private String letterOption;
	private boolean correct;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLetterOption() {
		return letterOption;
	}

	public void setLetterOption(String letterOption) {
		this.letterOption = letterOption;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

}
