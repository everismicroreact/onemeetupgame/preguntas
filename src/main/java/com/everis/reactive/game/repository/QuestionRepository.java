package com.everis.reactive.game.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.everis.reactive.game.entitty.dto.QuestionForm;

public interface QuestionRepository extends MongoRepository<QuestionForm, String> {
	

}
