package com.everis.reactive.game.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.reactive.game.entitty.dto.PlayerAnswerForm;
import com.everis.reactive.game.service.GameChallengesServiceContract;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/game")
public class GameChallengesAPIController {

	@Autowired
	GameChallengesServiceContract registerService;

	@GetMapping(value = "/challenge", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> requestForRandomChallenge() {
		try {
			
			return ResponseEntity.status(HttpStatus.OK).body(registerService.requestForAChallenge());
		} catch (RuntimeException exc) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Mono.just(exc.toString()));
		}
	}

	@PostMapping(value = "/challenge", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> validateAnswer(@RequestBody PlayerAnswerForm answer) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(registerService.validateChallengeAnswer(answer));
		} catch (RuntimeException exc) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Mono.just(exc.toString()));
		}
	}
	
}
